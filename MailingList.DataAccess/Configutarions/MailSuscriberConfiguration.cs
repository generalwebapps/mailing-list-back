﻿using MailingList.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MailingList.DataAccess.Configutarions
{
    internal class MailSuscriberConfiguration : IEntityTypeConfiguration<MailSuscriber>
    {
        public void Configure(EntityTypeBuilder<MailSuscriber> mailSuscriberBuilder)
        {
            mailSuscriberBuilder.ToTable("MailSuscribers");
            mailSuscriberBuilder.HasKey(m => m.Id);
            mailSuscriberBuilder.Property(m => m.Name).HasMaxLength(120).IsRequired();
            mailSuscriberBuilder.Property(m => m.LastName).HasMaxLength(200);
            mailSuscriberBuilder.Property(m => m.Email).HasMaxLength(200).IsRequired();
        }
    }
}
