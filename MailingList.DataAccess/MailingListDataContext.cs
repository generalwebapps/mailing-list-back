﻿using MailingList.DataAccess.Configutarions;
using MailingList.Domain;
using Microsoft.EntityFrameworkCore;

namespace MailingList.DataAccess
{
    public class MailingListDataContext : DbContext
    {
        public MailingListDataContext(DbContextOptions<MailingListDataContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new MailSuscriberConfiguration());

            AddMyFilters(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<MailSuscriber> MailSuscribers { get; set; }

        private void AddMyFilters(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MailSuscriber>().HasQueryFilter(m => m.IsDeleted == false);
        }
       
    }
}