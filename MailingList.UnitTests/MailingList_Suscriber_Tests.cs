using MailingList.DataAccess;
using MailingList.Domain;
using MailingList.ServiceData;
using MailingList.ServiceData.Infraestructure;
using Microsoft.EntityFrameworkCore;
using WebApps.Common.ServiceDataResponse;

namespace MailingList.UnitTests
{
    public class MailingList_Suscriber_Tests
    {
        MailingListDataContext _context;
        MailSuscriberService _mailSuscriberService;


        [SetUp]
        public void Setup()
        {
            _context = GetMemoryContext();
            _mailSuscriberService = new MailSuscriberService(_context);
        }

        [TestCase]
        public async Task CanAddSuscriber()
        {
            //Act
            var suscriber = new MailSuscriber
            { Name = "Salma", LastName = "Jacobo", Email = "karisal30@gmail.com" };
            
            var sr = await _mailSuscriberService.Add(suscriber);

            Assert.IsTrue(sr.Success);
        }

        [TestCase]
        public async Task CanNotAddRepeatedSuscriber()
        {
            //Act
            var suscriber1 = new MailSuscriber
            { Name = "Octavio", LastName = "Herrera", Email = "oherrera30@gmail.com" };

            var suscriber2 = new MailSuscriber
            { Name = "Octavio 2", LastName = "herrera 2", Email = "oherrera30@gmail.com" };

            await _mailSuscriberService.Add(suscriber1);
            var sr2 = await _mailSuscriberService.Add(suscriber1);

            //Assert
            Assert.IsInstanceOf<ServiceEntityDuplicated>(sr2);
        }

        MailingListDataContext GetMemoryContext()
        {
            var options = new DbContextOptionsBuilder<MailingListDataContext>()
            .UseInMemoryDatabase(databaseName: "InMemoryDatabase")
            .Options;
            return new MailingListDataContext(options);
        }
    }
}