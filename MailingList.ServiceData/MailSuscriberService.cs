﻿using MailingList.DataAccess;
using MailingList.Domain;
using MailingList.Dtos;
using MailingList.ServiceData.Helpers;
using MailingList.ServiceData.Infraestructure;
using WebApps.Common.Excepcion;
using WebApps.Common.ServiceDataResponse;

namespace MailingList.ServiceData
{
    public class MailSuscriberService : IMailSuscriberService
    {
        protected readonly MailingListDataContext _context;
        public MailSuscriberService(MailingListDataContext context)
        {
            _context = context;
        }


        public async Task<ServiceResponse> Add(MailSuscriber mailSuscriber)
        {
            var sr = new ServiceResponse();
            try
            {

                if (EmailExists(mailSuscriber.Email))
                    return new ServiceEntityDuplicated("suscriber");

                mailSuscriber.Email = mailSuscriber.Email.ToLower().Trim();

                _context.Add(mailSuscriber);
                await _context.SaveChangesAsync();

                return sr;
            }
            catch(ServiceException)
            {
                throw new ServiceException(sr.UserMessage);
            }
            catch (Exception e)
            {
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                sr = new ServiceOperationFailed("An error occurred trying to add the suscriber", e, GetType().Name, methodName);
                throw new ServiceException(sr.UserMessage);
            }
        }

        public async Task<IEnumerable<MailSuscriberDto>> GetMailSuscribers(MailSuscriberFilter mailSuscriberFilter)
        {
            try
            {
                var suscribers = _context.MailSuscribers.OrderBy(s => s.LastName).AsQueryable();

                if (!string.IsNullOrEmpty(mailSuscriberFilter.LastName))
                    suscribers = suscribers.Where(s => s.LastName.Contains(mailSuscriberFilter.LastName));

                if (mailSuscriberFilter.OrderByLastNameDesc)
                    suscribers = suscribers.OrderByDescending(s => s.LastName).ThenByDescending(s => s.Name);

                return await Task.Run(() => {
                    
                    var suscriberList = suscribers.Select(s => new MailSuscriberDto()
                    {
                        Id = s.Id,
                        LastName = s.LastName,
                        Name = s.Name,
                        Email = s.Email

                    }).ToList();

                    return suscriberList;
                });
            }
            catch (Exception e)
            {
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                var sr = new ServiceOperationFailed("An error ocurred trying to get suscribers", e, GetType().Name, methodName);
                throw new ServiceException(sr.UserMessage);
            }

        }


        private bool EmailExists(string email)
        {
            var sr = new ServiceResponse();
            try
            {
                email = email.Trim().ToLower();
                return _context.MailSuscribers.Any(m => m.Email == email);
            }
            catch (Exception e)
            {
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                sr = new ServiceOperationFailed("An error occurred trying to verify the email suscriber", e, GetType().Name, methodName);
                throw new ServiceException(sr.UserMessage);
            }
        }
    }
}