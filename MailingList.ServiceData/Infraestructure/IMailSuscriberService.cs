﻿using MailingList.Domain;
using MailingList.Dtos;
using MailingList.ServiceData.Helpers;
using WebApps.Common.ServiceDataResponse;

namespace MailingList.ServiceData.Infraestructure
{

    public interface IMailSuscriberService
    {
        Task<ServiceResponse> Add(MailSuscriber mailSuscriber);
        Task<IEnumerable<MailSuscriberDto>> GetMailSuscribers(MailSuscriberFilter mailSuscriberFilter);
    }
}
