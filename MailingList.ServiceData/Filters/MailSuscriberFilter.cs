﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailingList.ServiceData.Helpers
{
    public class MailSuscriberFilter
    {
        public bool OrderByLastNameDesc { get; set; }
        public string? LastName { get; set; }
    }
}
