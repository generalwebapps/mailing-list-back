﻿namespace MailingList.Domain.Infraestructure
{
    public interface ISoftDeleted
    {
        public bool IsDeleted { get; set; }
    }
}
