﻿using MailingList.Domain.Infraestructure;

namespace MailingList.Domain
{
    public class MailSuscriber : ISoftDeleted
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public bool IsDeleted { get ; set; }
    }
}