﻿using System.ComponentModel.DataAnnotations;

namespace MailingList.Api.ViewModels
{
    public class MailSuscriberViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "The name is required")]
        [MaxLength(120, ErrorMessage = "The Name must be less than {1} characters")]
        public string? Name { get; set; }

        [MaxLength(200, ErrorMessage = "The Name must be less than {1} characters")]
        public string? LastName { get; set; }

        [Required(ErrorMessage = "The email is required")]
        [MaxLength(200, ErrorMessage = "The email must be less than {1} characters")]
        public string? Email { get; set; }
    }
}
