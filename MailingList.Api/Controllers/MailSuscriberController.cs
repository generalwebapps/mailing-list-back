﻿using MailingList.Api.Helpers;
using MailingList.Api.ViewModels;
using MailingList.Domain;
using MailingList.Dtos;
using MailingList.ServiceData.Helpers;
using MailingList.ServiceData.Infraestructure;
using Microsoft.AspNetCore.Mvc;
using WebApps.Common;
using WebApps.Common.Excepcion;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MailingList.Api.Controllers
{
    [Route("api/email-suscriber")]
    [ApiController]
    public class MailSuscriberController : ControllerBase
    {
        private readonly IMailSuscriberService _mailSuscriberService;
        private readonly ControllerOperationFailed _controllerFailedReporter;

        public MailSuscriberController(IMailSuscriberService mailSuscriberService)
        {
            _mailSuscriberService = mailSuscriberService;
            _controllerFailedReporter = new ControllerOperationFailed();

        }

        // GET: api/email-suscriber
        [HttpGet]
        public async Task<PayLoad<IEnumerable<MailSuscriberDto>>> GetMailSuscribers([FromQuery] MailSuscriberFilter mailSuscriberFilter) 
        {
            var payLoad = new PayLoad<IEnumerable<MailSuscriberDto>>();
            try
            {
                var suscribers = await _mailSuscriberService.GetMailSuscribers(mailSuscriberFilter);
                payLoad.Data = suscribers;
            }
            catch (ServiceException e)
            {
                payLoad.HttpCode = (short)HttpResponseCode.ServerError;
                payLoad.Message = e.Message;
            }
            catch(Exception e)
            {
                payLoad.HttpCode = (int)HttpResponseCode.ServerError;
                payLoad.Message = "An error occurred trying to get suscribers, try again later or contact support department";
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _controllerFailedReporter.RegisterFailedOperation(payLoad.Message, e, GetType().Name, methodName);
            }
            return payLoad;
        }

     

        // POST api/email-suscriber
        [HttpPost]
        public async Task<PayLoad<MailSuscriberViewModel>> AddSuscriber([FromBody] MailSuscriberViewModel mailSuscriberViewModel)
        {
            var payLoad = new PayLoad<MailSuscriberViewModel>();

            try
            {
                if (!ModelState.IsValid)
                {
                    payLoad.Message = "The validations did not pass, check the captured information";
                    payLoad.HttpCode = (int)HttpResponseCode.BadRequest;
                    payLoad.Data = null;
                }

                var newSuscriber = new MailSuscriber
                {
                    Name = mailSuscriberViewModel.Name,
                    LastName = mailSuscriberViewModel.LastName,
                    Email = mailSuscriberViewModel.Email
                };

                var sr = await _mailSuscriberService.Add(newSuscriber);
                if (sr.Success)
                {
                    mailSuscriberViewModel.Id = newSuscriber.Id;
                    payLoad.Data = mailSuscriberViewModel;
                    payLoad.HttpCode = (short)HttpResponseCode.Created;
                }

                payLoad.Message = sr.UserMessage;
            }
            catch (ServiceException e)
            {
                payLoad.HttpCode = (int)HttpResponseCode.ServerError;
                payLoad.Message = e.Message;
            }
            catch (Exception e)
            {
                payLoad.HttpCode = (int)HttpResponseCode.ServerError;
                payLoad.Message = GlobalVariables.GeneralErrorMessage;
                var methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
                _controllerFailedReporter.RegisterFailedOperation("An error occurred trying to add suscriber", e, GetType().Name, methodName);
            }
            return payLoad;
        }

    }
}
