﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApps.Common.ServiceDataResponse
{
    public class ServiceResponse
    {
        public bool Success { get; set; }
        public string UserMessage { get; set; }
        public string MessageType { get; set; }
        public string SystemErrorMessage { get; set; }
        public List<string> ValidationErrors { get; set; }

        public ServiceResponse()
        {
            // By Default
            Success = true;
            UserMessage = "Operation ended successfully";
            MessageType = "success";
            ValidationErrors = new List<string>();
        }
    }
}
