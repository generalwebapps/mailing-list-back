﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApps.Common.ServiceDataResponse
{
    public class ServiceEntityDuplicated : ServiceResponse
    {
        public ServiceEntityDuplicated(string entityName)
        {
            Success = false;
            UserMessage =$"The {entityName} that you are trying to add is already in the database.";
        }
    }
}
