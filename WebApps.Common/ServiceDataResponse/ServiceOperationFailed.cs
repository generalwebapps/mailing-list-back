﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApps.Common.Excepcion;

namespace WebApps.Common.ServiceDataResponse
{
    public class ServiceOperationFailed : ServiceResponse
    {
        string? EntityName { get; set; }
        string? OperationName { get; set; }
        DateTime OperationFailedDate { get; set; }
        string? InnerException { get; }
        private string? Layer { get; }
        private int LineNumber { get; set; }
        public ServiceOperationFailed()
        {

        }

        public ServiceOperationFailed(string userMessage)
        {
            UserMessage = userMessage;
        }

        public ServiceOperationFailed(string userMessage, Exception exception, string serviceName = "", string operationName = "")
        {
            OperationFailedDate = DateTime.Now;
            Success = false;
            MessageType = "danger";
            UserMessage = userMessage;
            EntityName = serviceName;
            OperationName = operationName;
            SystemErrorMessage = exception.Message;
            //Si no se especifica la capa, por default es en la capa de servicios
            Layer = "service";
            if (exception.InnerException != null)
                InnerException = exception.InnerException.Message;
            LineNumber = exception.GetLineNumber();
            RegisterFailedOperation();
        }
        void RegisterFailedOperation()
        {
            try
            {
                var webRoot = $"{GlobalVariables.WebRoot}/Logs";
                var fileName = Path.Combine(webRoot, "ErrorLogService.txt");

                if (!Directory.Exists(webRoot))
                    Directory.CreateDirectory(webRoot);

                if (!File.Exists(fileName))
                    using (var streamWriter = new StreamWriter(fileName, true, Encoding.UTF8))
                        streamWriter.WriteLine($"DateTimeError;Entity;Operation;Layer;LineCode;Exception;InnnerException");

                using (var streamWriter = new StreamWriter(fileName, true, Encoding.UTF8))
                    streamWriter.WriteLine($"{OperationFailedDate:yyyy-MM-dd HH:mm:ss};{EntityName};{OperationName};{Layer};{LineNumber};{SystemErrorMessage};{InnerException}");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
