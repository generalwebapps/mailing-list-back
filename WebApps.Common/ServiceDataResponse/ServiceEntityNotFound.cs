﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApps.Common.ServiceDataResponse
{
    public class ServiceEntityNotFound : ServiceResponse
    {
        public ServiceEntityNotFound()
        {
            Success = false;
            UserMessage = "Requested entity not found, operation could not be performed";
        }
    }
}
