﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApps.Common.Excepcion
{
    public class ServiceException : Exception
    {
        public ServiceException(string userMessage) : base(userMessage)
        {

        }
    }
}
