﻿namespace WebApps.Common
{
    public class PayLoad<T>
    {
        public PayLoad()
        {
            Message = "Operation Success";
            HttpCode = (short)HttpResponseCode.Ok;
        }

        public short HttpCode { get; set; }
        public string Message { get; set; }
        public T? Data { get; set; }

    }
}