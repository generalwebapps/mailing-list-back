﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApps.Common
{
    public static class GlobalVariables
    {
        public static string WebRoot = string.Empty;

        public static string GeneralErrorMessage = "An error executing the operation, try again later or contact support department";
    }
}
